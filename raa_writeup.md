# Risk Assessment Write Up

* Improved Architecture
    - Onion architecture
        * The layers
        * Some samples
        * How it would positively affect
    - Domain Models
        * How to do properly
        * How it would make better architecture

### Introduction

This write up is an attempt to document and explain what went wrong with the Risk Assessment project.  Risk Assessment should have been a relatively simple CRUD application but it suffered from over architecture which inflated after an attempt at refactoring.  It became fragile and caused a lot of fear and uncertainty within the teams.  Despite it being a greenfield project it is already in a way legacy due to maintainability issues and some developers don't like working on it already.

The intent of this write up is to hopefully highlight some of the issues we encountered but didn't notice up front.  Hopefully next project can take some of this information ane make use of it before we recreate the same problems.


![Over engineered](https://github.com/andrewgoacher/blogposts/blob/master/overengineered.jpg?raw=true)

### The Non-Technical part

Despite this being a technical retrospective, I think it's important to stress some non-technical aspects.

The team did not have a clear domain understanding before starting this project.  There was a lot of confusion on terms should as "Template" or "Risk Assessment" This lack of terminology clarity meant that objects we created were weak before we used them.

The decision to refactor was fuelled (we thought) by urgency and necessity.  We wanted to add versioning into the system but the consensus was an entire architecture refactor in the database.  I think the architecture we came up with was OK, but obviously the architecture of the rest of the application caused issue here.  I would add that we should have maybe pushed for a less invasive introduction to versioning. 

Downing tools!

We should have made an effort from all levels to stop the refactor in its tracks when we first realised the scope of the issue.  This is more a reflective learning opportunity we shouldn't repeat.  As a team we need to put our foot down when we realise something is going awry - it's a hard thing to do but it I think is a requirement.  Had we stopped initially with Risk Assessment we could have come up with a different database structure or prevented more changes while we planned a new approach.

Not speaking out - 

As a team, developers need to know it's OK to speak out when something stinks.  The person writing the code can sometimes be a little too close to the changes.  As a team we all need to share in the accountability and make sure we stop an avalanche in its tracks.

### Current Architecture

The current architecture is supposed to be an implementation of the CQRS pattern augmented with Repository pattern usage.  The structure uses several layers from command to Db.

![cqrs pattern](https://github.com/andrewgoacher/blogposts/blob/master/cqrs-single-model.png?raw=true)

The CQRS framework is a separate library that is tightly coupled with an infrastructure project.  The infrastructure project is quite tightly coupled to Entity Frameworks implementation.  This is because initially we accidentally bled EFs async enumerators into Risk Assessment.  To compensate for this, an interface containing Async enumeration methods was created and implementations created for each infrastructure type.
The infrastructure is a wrapper around a repository pattern but very closely models Entity Framework again.

The coupling isn't really needed here.  I don't think it's actually used.  

The CQRS project itself has exposes two concepts; a Command and a Query.
The command relies on a Domain Service, which is an empty type declaration used just to enforce types in some Generic helper methods.
The query relied on a Query implementation with a similar empty type.

![command layer diagram](https://github.com/andrewgoacher/blogposts/blob/master/command-layer-diagram.png?raw=true)

In a perfect world the above diagram would be relatively clean.
The database repository acts more like a Table class.  The infrastructure project contains a helper method to register classes - DI will then resolve an ```IRepository<TEntity>``` class into an actual concrete class which allows us to add, update and remove the said entity from a database.

The repository layer above it is misused.  It's used as a mirror of the infrastructures repository.  Very little is done with this layer that is needed aside from a couple of extended methods for specifically tricky find methods.

The domain service (or query service) layer takes several repositories (which I'll refer to as tables for this part because that's essentially what they are) and applies logic to create objects that the domain level can use.

The command layer takes (sometimes) several domain services and applies logic to command arguments.

From the diagram, you can see that the models for the database are named "DB Model".  It's a database model not a domain model.  The domain model is constructed later on, in Query or Domain services most of the time. 

In code, we would have had something like the following;


__NOTES__ : Persistence ignoance.

```cs

interface IEntity
{
    // no real implementation here
}

interface IRepository
{
    Task SaveAsync(CancellationToken token);
    IQueryable<T> AsQueryable<T>() where T : class, IEntity;
}

interface IQuery<TEntity> : IQueryable<TEntity> 
where TEntity : IEntity
{
}

interface IRepository<TEntity> : IRepository
    where TEntity : class, IEntity
{
    TEntity Add(TEntity entity);
    TEntity Remove(TEntity entity);
    TEntity Update(TEntity entity);

    Task<TEntity> FindByIdAsync(int id, CancellationToken token);
}

abstract class BaseRepository<TEntity> : IRepository<TEntity>
    where TEntity : class, IEntity
{
    protected IRepository<TEntity> Repository {get;}

    protected BaseRepository(IRepository<TEntity> repo)
    {
        Repository = repo;
    }

    TEntity Add(TEntity entity)
    {
        return Repository.Add(entity);
    }

    // ... The rest of the implementation
}

class Repository<TEntity> : IRepository<TEntity> {} // ...
class Query<TEntity> : IQuery<TEntity> {} // ...

```

The above code is a very short whirlwind tour of how we would go about setting up the project.  Below is a single domain service highlighting how the repositories are being used.


```cs

class Table1Entity
{
    public int Id {get;set;}

    public string ShortName {get;set;}

    public DateTime? Created {get;set;}
}

class Table2Entity 
{
    public int Id {get;set;}

    public int ExternalId {get;set;}

    public string ShortName {get;set;}

    public bool IsAwesome {get;set;}
}

class SomeDomainObject
{
    public string Name {get;set;}

    public bool IsAwesome {get;set;}

    public DateTime? Created {get;set;}
}

interface ITable1Repository : IRepository<Table1Entity> {}
interface ITable2Repository : IRepository<Table2Entity> {}

class SomeDomainService : IDomainService
{
    private readonly ITable1Repository table1Repository;
    private readonly ITable2Repository table2Repository;

    // .. some more implementation

    public async Task<SomeDomainObject> GetSomeDomainObjectByName(string name)
    {
        return from t1 in table1Repository
        join t2 in table2Repository on t1.Id equals t2.ExternalId
        select new SomeDomainObject {
            Name = t1.Name + " " + t2.Name,
            IsAwesome = t2.IsAwesome,
            Created = t1.Created ?? t2.Created
        }.ToListAsync();
    }
}


```

Some things to note above.
Firstly there is some dependency usage - the class requires use of 2 repositories.  These are named, not subtly, after the tables they represent.  This is modelled from actual usage.  There is a large issue here that the database table design or schema, has bled into the actual project.  This domain service required intimate knowledge of how the Database is represented.

This is bad for several reasons:

1. If the schema changes, it requires a __lot__ of changes to not only the DB models, but potentially the repositories (if they do more than just map a table) but also to the Domain layer.  The domain service should concern itself with domain objects, it shouldn't care about the database

2. Testing this domain object now requires intimate knowledge of the DB also.  For some testing this might make sense, but if you want to test a small piece of logic in complete isolation it becomes quite complex, it requires a lot of setup.  The problem with a complex test is time will always beat a developer into laziness and complacency.  They will repeat set ups from other tests, mixing contexts or propagating bad tests.  Again we had this issue.  Integration tests had very verbose, very complicated setup and didn't test for anything of value beyond "did the test throw a bad error".

3. The infrastructure project intended to abstract away the database.  This has quite tightly coupled the database to the system:

This one requires a little more explanation as it's multifaceted.
Firstly it's not usual to completely rip out a database implementation once it's in production.  It's costly and prone to all sorts of error, usually it'll be patched and mended and someone will pray that it holds up as well as it can.
That being said, at some point it might be the thing to do, or the database will be augmented with another for some reason.  In our case this couldn't easily happen.  You may notice the ToListAsync() call at the bottom of ```GetSomeDomainObjectByName()``` this was intentional.  Entity Framework contains this extension method and it's a great use, if using Entity Framework.  It might not exist in other implementations.  NHibernate for instance may not have a direct implementation of this and rely on the developer to asynchronously enumerate themselves.

To get around this a wrapper was created.  The problem with this is that switching to another framework will now be rife with more uncertainty at a stage in development when the team already knows it will be uncertain enough!  This means it's unlikely that anything will change.  Chances are if another database is used it would require another large refactor to rip out this implementation.

Again with the DB, this relies on LINQ to sql implementations.  Not all DBs provide support for this, in fact, the code sample above shows a very distinct relational structure.  Risk Assessments could very easily have been stored in a json based NoSQL storage system.  A complete risk assessment would have mapped very well to a json object, especially as during development, the entire "Risk Assessment Wizard" represented the template in JSON.  Had this been a considered factor we would again have needed to create or update Risk Assessment quite thoroughly.

-----------------------------------------

Another issue to note, which was brought up briefly earlier is that the Repository pattern was somewhat misused. 

This was a misunderstanding communicated through a piece of sample code.  It's actually a large part of the failure of the refactor.  The infrastructure provided a repository implementation - this would have been the table mapping.  The Repositories in Risk Assessment should have been __domain repositories__.  This is an important distinction that will be highlighted in the next section.

Had the repositories in Risk Assessment been created as Domain Repositories, it's likely a lot of the pain of the refactor would have been mitigated.  This would be a good stepping off point.  In hindsight this would have been _enough_ of a change to make.  A change that is actually somewhat possible in the current design.  The changes would still have some uncertainty but it would require no significant refactoring and would make life easier for everyone going forward.  But it would be treating a symptom, not the disease.

### Improved Architecture

To talk about an improved architecture I'm going to go through 2 concepts and explain how they should have been used for Risk Assessment.

The 2 concepts are; Domain Models and an Onion/Layered architecture.

The first is __Domain Models__.  A __Domain Model__ is a conceptual model of a piece of the system.  This is a model that a product owner, developer, tester, director or any other stake holder can see and understand.  The model would be something tangible within the system.  The idea here is that a product owner or stakeholder wouldn't care about the _how_ of a domain model so much.  They wouldn't need to know whether it was stored in one table or many, or none at all.  They would just be able to understand that when I used a certain _term_ when explaining something that it mapped to this model.

An example here would be a _Risk Assessment Template_.  There were many terms tossed around during the development of Risk Assessment that got switched and mixed and misunderstood.  The 2 to look at here are _Risk Assessment_ and _Template_.  These were used interchangeably and also had very different meaning to different stakeholders which would have been a pretty huge red flag.  So for this example I will define a _Risk Assessment Template_ as the template structure of a _Risk Assessment_ that can be assigned to a _Client Location_.  There would of course be opportunity to give this much more description and meaning but for this example I think that's enough.  The business can see that a _Risk Assessment Template_ is __not__ a _Risk Assessment_ but instead the template of one which can be modified and assigned out.

During this initial stage of development we're not going to be concerned with versionning or with modifications to a _Risk Assessment Template_.

A Risk Assessment Template has the following characteristics.

* A Name 
* A description
* A list of exposed categories of people (to be named ExposedPeopleGroups)
* An optional image to represent the risk
* A collection of hazards (to be described)
* A collection of control measures (to be described)
* A set of likelihoods of hazards occuring
* A set of amendments to the likelihoods if control measures are enforced.

From this we can also determine that the __ExposedPeopleGroups__ will be a set of predefined groups that a user can select.  It's required to select at least one, but multiple or all are also possible.

From this the following models can be created in code.

```cs 

Dictionary<int, string> ExposedPeopleGroups = 
{
    {1, "Colleagues"},
    {2, "Contractors"},
    {3, "Visitors / Guests" },
    {4, "Members of the public"}
};

class Hazard {}

class ControlMeasure {}

class HazardLikelihood 
{
    public Hazard Hazard {get;set;}

    public short Likelihood {get;set;}
}

class HazardAmendedLikelihood 
{
    public HazardLikelihood {get;set;}

    public short AmendedLikelihood {get;set}
}

// note: this does not represent an implementation that is live, it's a 5 min thought experiment
class RiskAssessmentTemplate
{
    public string Name {get;set;}

    public string Description {get;set;}

    public Image? OptionalImage {get;set;}

    public IReadOnlyList<int> ExposedPeople {get;set;}

    public IReadOnlyList<Hazard> Hazards {get;set;}

    public IReadOnlyList<ControlMeasure> ControlMeasures {get;set;}

    public IReadOnlyList<HazardLikelihood> Likelihoods {get;set;}

    public IReadOnlyList<HazardAmendedLikelihood> AmendedLikelihoods {get;set;}
}

```

The above code represents a domain that should have been described and agreed upon by __all__ stakeholders.  The benefit of this is a beginning list of shared terminology.  At any meeting, if someone says _Risk Assessment Template_ there should be mutual understanding.  This is especially important for the developers who might want to find that model and see how it's been represented.

The above code will also be persisted in a SQL database.  To do this a database design will be required.

![Table design](https://github.com/andrewgoacher/blogposts/blob/master/ras_design.PNG?raw=true)

The above table design shows the same information, but it's stored differently.  This is __important__.  How the data is stored is irrelevant to the majority of the stake holders (within reason), but this information is still important.  At some point, during data store, or data retrieval a transformation will need to take place to get from A to B regardless of which way that transformation occurs.  We would need to model this data in code too, but it's obviously not a __domain model__ now.  This code would be a data model.

Before jumping into more code however I'd like to talk about the second point, _Onion Layer Architecture_.  
The problem with Risk Assessment is that the Database drives the entire application - it's the core of the system.  It probably shouldn't be.  The entire application is aware of the database, is aware of the type and the structure of the data base and has allowed it's influence to permeate through every layer.

![command layer diagram](https://github.com/andrewgoacher/blogposts/blob/master/command-layer-diagram.png?raw=true)

A repeat of this diagram here is necessary I think.  Firstly note that the __db model__ not the domain model sits sideways throughout every layer.  This isn't quite true, to be fair there are more specific models constructed and used throughout Risk Assessment but it's a fair approximation.  It's almost a 1 to 1 mapping from the data to the UI in a lot of cases - with some combinations and aggregations used to bring related models together.
I once tried to state that Risk Assessment _almost_ has an Onion Architecture about it, like it was close but just missed.  I was told unequivocally that it wasn't and upon examination I agree.  It's as similar to an Onion Layered architecture as an exposion is to an implosion.  Close but not quite.

![Onion Architecture](https://github.com/andrewgoacher/blogposts/blob/master/onion_architecture.png?raw=true)

The following diagram describes an Onion Architecture.

My understanding of this architecture (boiled down a little) is as follows.

The centre of this diagram, the _domain entities_ is pure.  Nothing else touches this core.  
How this is laid out in project structure is a stylistic choice but it should contain the domain models for your project.  Whether these are anemic domain models (mere POCOS) or richer domain models with self contained validations and functionality won't matter - but they should have no other dependencies.

This section is __very important__ this represents how the business views something.  The fact is that while things might change in a system, and new requirements may surface over time - these definitions shouldn't change too much - if ever.

If you do have domain specific logic here, it's very easy to test as it has no external dependencies.  

For example - a Risk Assessment Template may have the following requirements:

* __Must__ have at least one hazard
* Hazard Names __must be unique within that template__
* Hazards __should have a default likelihood of 10 out of 10__ - this is for the user to be highlighted that it's a risk and will enforce actual checking when creating hazards.

We can create this logic within the Risk Assessment Template class as so.

``` cs

class RiskAssessmentTemplate
{
    public string Name {get;set;}

    public string Description {get;set;}

    public Image? OptionalImage {get;set;}

    public IReadOnlyList<int> ExposedPeople {get;set;}

    public IReadOnlyList<Hazard> Hazards {get { return hazards; }}

    public IReadOnlyList<ControlMeasure> ControlMeasures {get;set;}

    public IReadOnlyList<HazardLikelihood> Likelihoods {get;set;}

    public IReadOnlyList<HazardAmendedLikelihood> AmendedLikelihoods {get;set;}

    private List<Hazard> hazards = new List<Hazard>();

    public void AddHazard(Hazard hazard)
    {
        if(hazards.Any(h => h.Name.Equals(hazard.Name, StringComparison.OrdinalIgnoreCase)))
        {
            throw new HazardNameExistsException(Name, hazard.Name);
        }

        if(hazard.Likelihood.HasValue == false)
        {
            hazard.Likelihood = 10;
        }

        hazards.Add(hazard);
    }

    public void Validate()
    {
        if(!hazards.Any())
        {
            throw new MustHaveHazardsException(Name);
        }
    }
}

```

The following changes were made to the Risk Assessment Template domain model.
1. Replace the getter/setter of Hazards with a getter to an internal list. - Enforce adding hazards through a method
2. AddHazard method contains a unique name validation
3. AddHazard will default an empty likelihood to 10
4. Validate will error if no hazards are present.

This is now testable.

```cs

[Test]
public void AddHazard_DuplicateHazard_ThrowsException()
{
    // Arrange
    var template = new RiskAssessmentTemplate();
    const string duplicateName = "abc";

    template.AddHazard(new Hazard { Name = duplicateName});

    // Act
    // Assert

    Assert.ThrowsException<HazardNameExistsException>(() => {
        template.AddHazard(new Hazard { Name = duplicateName});
    });
}


[Test]
public void AddHazard_NoLikelihood_DefaultsTo10()
{
    // Arrange
    var template = new RiskAssessmentTemplate();

    // Act
    template.AddHazard(new Hazard());

    // Assert
    var hazard = template.Hazards.Single();
    Assert.Equals(10, hazard.Likelihood);
}


[Test]
public void AddHazard_HasLikelihoodOf5_DoesntDefaultTo10()
{
    // Arrange
    var template = new RiskAssessmentTemplate();
    const int likelihood = 5;

    // Act
    template.AddHazard(new Hazard {Likelihood = likelihood});

    // Assert
    var hazard = template.Hazards.Single();
    Assert.Equals(likelihood, hazard.Likelihood);
}


[Test]
public void Validate_NoHazards_ThrowsException()
{
    // Arrange
    var template = new RiskAssessmentTemplate();

    // Act
    // Assert

    Assert.ThrowsException<MustHaveHazardsException>(() => {
        template.Validate();
    });
}

```

These tests represent distinct business rules. They are in no way dependent on the rest of the system (although the examples __are__ very simple).  Each of these represent a business rule that a product owner and tester can verify.

The next layer above this will be a repository layer - this layer knows the shape of the domain and should be a __domain repository__.  At this layer the majority of what exists will be contracts / interfaces.

In this example we will create 2 repositories.

One will be the __RiskAssessmentTemplateRepository__.  The business requirements need a way to find a specific _Risk Assessment Template_.  The other will be a __RiskAssessmentTemplateHazardRepository__.  The business requirements need a way to get a hazard or list of hazards for a specific Risk Assessment Template.

```cs

/*
    Some Assumptions:
    1. We're not dealing with async here
    2. This system is tenanted - we know which user we're dealing with here.
*/
interface IRiskAssessmentTemplateRepository:  IRepository<Domain.RiskAssessmentTemplate>
{
    Domain.RiskAssessmentTemplate GetTemplateByName(string name);

    IEnumerable<Domain.RiskAssessmentTemplate> GetTemplates();
}


interface IRiskAssessmentTemplateHazardRepository:  IRepository<Domain.Hazard>
{
    Domain.Hazard GetHazardByName(Domain.RiskAssessmentTemplate template, string name);

    IEnumerable<Domain.Hazard> GetHazards(Domain.RiskAssessmentTemplate template);
}

```

Some things to note about this layer.

1. It's not concrete.  This is a contract that upper layers can implement and use.  It will honor the Dependency Inversion principle.
2. We're dealing with instances or enumerables.  There are no queryables here, once we have something it's there to use.  The domain service might take bits from multiple repositories but at this stage that's not a concern.
3. There are no ids or anything.  Here I might want a hazard, but I know it relates to a _Risk Assessment Template_  __how__ it relates is up to an implementation later on.

Here I'm also not dealing with implementations of Add, Update or Delete etc.  Again those can be abstracted away (in this case, the Base IRepository interface handles those).

Above this layer (or at the same one depending on how you view it), will be a domain service layer.

This layer again will contain mostly interfaces.  It's about defining the behaviour, not the implementation.

```cs

interface IRiskAssessmentTemplateService
{
    void Add(Domain.RiskAssessmentTemplate template);
}

```

The business might have a requirement that you can add a hazard to a risk assesssment template.  While we could do this directly with the repository - they state that should it fail with a predefined error code.
this is logic that is unfit to exist inside a repository or the domain itself.

Not everything will need a service.  Sometimes the domain model is enough, and so then will be the repository.  Adding an "Add" method purely to call ```repository.Add(x);``` to me seems redundant.  However, if you want additional functionality or validations then it makes sense to expose an Add method at the service layer.

The last layer brings everything together - this layer contains your interfaces to external sources.
This could be a UI, API or even test environment.  It also contains implementations.

This is a different concept to Risk Assessment as these concerns were initially central.  In this scenario though, the domain and contracts are all inside the inner circles.  In this example, for instance, had the database changed it would definately have been restricted to a segment of the outer layer.

in this segment we may have the following

```cs

class RiskAssessmentTemplateDTO
{
    // details ommitted    
}

class RiskAssessmentTemplateService : IRiskAssessmentTemplateService
{
    IRiskAssessmentTemplateRepository repository;

    /// implementation details
    public void Add(Domain.RiskAssessmentTemplate template)
    {
        repository.Add(template);
    }

}

class RiskAssessmentTemplate
{
    int Id {get;set;}
    int ImageId {get;set;}
    string Name {get;set;}
    string Description {get;set;}
}

class Hazard 
{
    int Id {get;set;}
    string Name {get;set;}
    // other parts omitted
}

// other models omitted

class EFRiskAssessmentTemplateRepository : IRiskAssessmentTemplateRepository
{
    void Add(Domain.RiskAssessmentTemplate template)
    {
        var rat = Context.RiskAssessmentTemplates.Add(new RiskAssessmentTempalte
        {
            Name = // ...
        });

        Context.RiskAssessmentTemplates.SaveAll();

        Context.Hazards.Add(new Hazard {
            RiskAssessmentTemplateId = rat.Id,
            // ...
        });

        Context.Hazards.SaveAll();
    }
}

class RiskAssessmentTemplateController
{
    IRiskAssessmentTemplateService service;

    public ActionResult AddRiskAssessmentTemplate([FromBody] RiskAssessmentTemplateDTO ratDTO)
    {
        try
        {
            var riskAssessmentTemplate = new Domain.RiskAssessmentTemplate();
            // some stuff
            riskAssessmentTemplate.Validate();

            service.Add(riskAssessmentTemplate);
        }
        catch(/* specific exceptions*/) {}
    }
}

```

The above code is a rough implementation of how this would all work under the onion architecture.

The problems faced with Risk Assessment were numerous but the boil down roughly to 3 main issues.

1. Domain not understood - this would enforce more understanding if we started with the domain logic.  In this example I made the domains a little richer which is optional but highlights (I think) this better.  You need to understand the domain __in isolation__ before you can code against it.
2. Testability - the original code relied heavily on Table structure.  This was a nightmare.  There were a very great many discussions around Mocks, Stubs and Fakes which were valuable but came about because we had to find ways of coping with leaky abstractions in the table design.  This permeated __every__ section of the code.  We dealty with Linq to SQL directly in multiple layers and couldn't avoid it.  People wrote shoddy tests because it was easier and there was no faith in the original implementation __or__ the efforts during refactoring.
The refactor touched every layer of code (and thus every layer of test) so this uncertainty bled through.  All tests passed but the months following were frought with behaviour issues and things broke down in ways the tests didn't cover.  Business logic which __should not be affected by the refactor__ were affected.
With the above changes a few things are made easier. First you only care about implementation in the top layer, this layer __integrates__ everything else.  We could have written better integration tests here. and only here.
Other layers are mostly interfaces which on their own are redundatn to test.
The domain layer is either a set of POCOs or self testing.
3. Refactorability (I made the word up, remeber this during my nobel prize ceremony): Changing the database schema should have been a red flag.  It  was, there were meetings.
but we chose to pursue the refactor and it bled into every aspect of the code.  This was ultimately a failure of planning and communication but it was completed leading to an entirely new architecture which was more confusing and more prone to error.

Had the outer layer been the onyl one to suffer it would be almost trivial to change.   With the majority of the implementations here for various interfaces it would have been in one place.  With the repositories dealing with domain objects only it would likely have been restricted to the concrete repositories only.  For any of the other classes to change, it would represent a behavioural change of the system which would have then beena  valid reason for multiple tests to fail.