# This is the real shit

Basic infrastructure implementation:

Notes
----

* Had to generate an AsyncQueryable wrapper (Entity Framework provides ToListAsync() etc)
* 

```cs

interface IEntity
{
    // no real implementation here
}

interface IRepository
{
    Task SaveAsync(CancellationToken token);
    IQueryable<T> AsQueryable<T>() where T : class, IEntity;
}

interface IQuery<TEntity> : IQueryable<TEntity> 
where TEntity : IEntity
{
}

interface IRepository<TEntity> : IRepository
    where TEntity : class, IEntity
{
    TEntity Add(TEntity entity);
    TEntity Remove(TEntity entity);
    TEntity Update(TEntity entity);

    Task<TEntity> FindByIdAsync(int id, CancellationToken token);
}

abstract class BaseRepository<TEntity> : IRepository<TEntity>
    where TEntity : class, IEntity
{
    protected IRepository<TEntity> Repository {get;}

    protected BaseRepository(IRepository<TEntity> repo)
    {
        Repository = repo;
    }

    TEntity Add(TEntity entity)
    {
        return Repository.Add(entity);
    }

    // ... The rest of the implementation
}

```

Some work on top of this for Repository.cs and Query.cs (belongs in EF project)

```cs

class Repository<TEntity> : IRepository<TEntity> {} // ...
class Query<TEntity> : IQuery<TEntity>

```

* These 2 are specific implementations for Entity Framework.
* Registering an Entity for a IRepository will give you a Repository and a Query object as dependency injectibles.
* These use AsQueryable<T> which returns IQueryable<T> - necessary because we're still dealing with the underlying table here - harder to mock out and unit test.
* Couldn't easily use ToListAsync (from entity framework) - this is EF specific but bled into our codebase (leaky abstraction)
* Infrastructure hides away the DbContext for the EF Repository.  Not necessarily a bad thing I guess. but harder to make changes specific to EF without updating that project too.
* Repositories are all Direct table references.  No abstractions at all.  Dealing with these inside of domain services.

Example of initial example

```cs

class BlogPost : IEntity
{
    public int Id {get;set;}

    public string Title {get;set;}

    public string Description {get;set;}

    public string Content {get;set;}

    public DateTimeOffset? Published {get;set;}

    public DateTimeOffset? LastEdited {get;set;}
}

interface IBlogPostRepository : IRepository<BlogPost>
{
    Task<IEnumerable<BlogPost>> FindByPublishDateAsync(DateTimeOffset dt, CancellationToken token);
}

class BlogPostRepository : BaseRepository<BlogPost>, IBlogPostRepository
{
    public BlogPostRepository(IRepository<BlogPost> repository) : base(repository)
    {
    }

    public Task<IEnumerable<BlogPost>> FindByPublishDateAsync(DateTimeOffset dt, CancellationToken token)
    {
        return (from r in AsQueryable<BlogPost>()
                where // some criteria
                select r)
                // EF implementation 
                // Requires a wrapper
                .ToListAsync(token);
    }
}

```

Notes:

* Repository property on Base Property not used
* AsQueryable() / Repository - which to use.
* 

Looking at this objectively it's not far off what you'd want - you need to make some decisions
1. If I wanted to change the infrastructure, how painful would that be?
2. 

The sample looks innocent, which was part of the original issue - without us considering what we considered a repository and also what we considered the responsiblities of the repository, we made a massive understanding.

Let's look first at a more complicated example.

```cs

class BlogPost : IEntity
{
    public int Id {get;set;}

    public string Title {get;set;}

    public string Description {get;set;}

    public string Content {get;set;}

    public DateTimeOffset? Published {get;set;}

    public DateTimeOffset? LastEdited {get;set;}

    public int UserId {get;set;}
}

class User : IEntity
{
    public int Id {get;set;}

    public string FName {get;set;}

    public string LName {get;set;}
}

class BlogViewModel 
{
    public string Title {get;set;}

    public string Description {get;set;}

    public string Content {get;set;}

    public DateTimeOffset? Published {get;set;}

    public DateTimeOffset? LastEdited {get;set;}

    public string Author {get;set;}
}

class BlogService 
{
    readonly IRepository<BlogPost> blogRepo;
    readonly IRepository<User> userRepo;

    BlogService(IRepository<BlogPost> blogRepo, IRepository<User> userRepo)
    {
        this.blogRepo = blogRepo;
        this.userRepo = userRepo;
    }

    async Task<IEnumerable<BlogViewModel>> GetPostsByAuthor(string author, CancellationToken token)
    {
        return await (from b in blogRepo
                      join u in userRepo on b.UserId equals u.Id
                      select new BlogViewModel{
                          Title = b.Title,
                          Description = b.Description,
                          Content = b.Content,
                          Published = b.Published,
                          LastEdited = b.LastEdited,
                          Author = $"{u.FName} {u.LName}"
                      }).ToListAsync(token);
    }
}

```

Notes:

* Service exposes table structure
* A Change to infrastructure requires a change to the service
* We have to implement ToListAsync for any form of Repository
* Example above uses multiple repos but could look like this

```cs

class BlogService 
{
    readonly IRepository<BlogPost> blogRepo;

    BlogService(IRepository<BlogPost> blogRepo)
    {
        this.blogRepo = blogRepo;
    }

    async Task<IEnumerable<BlogViewModel>> GetPostsByAuthor(string author, CancellationToken token)
    {
        return await (from b in blogRepo
                      join u in blogRepo.AsQueryable<User>() on b.UserId equals u.Id
                      select new BlogViewModel{
                          Title = b.Title,
                          Description = b.Description,
                          Content = b.Content,
                          Published = b.Published,
                          LastEdited = b.LastEdited,
                          Author = $"{u.FName} {u.LName}"
                      }).ToListAsync(token);
    }
}

```

This sample is a bit cleaner.  In regards to usage of DI.

It hides that fact though - again the sample is small, and relatively simple.

This could get much mroe complex as the system grows.  This adds to complexity.

# Lets talk unit tests

Firstly we're talking the "traditional" way of doing .net unit tests.  

We're going to try and test individual units of code (class methods).

What we need to do here.

* Blog Service needs to know about the BlogRepo and the User Repo.
* Blog Service needs to have a lot of information about how our implementation of the data store actually stores the Blog and User data.
* Mocking IQueryables is painful

```cs

// Generic (ish) implementation of a stubbed query system
public class QueryStub<T> : IQuery<T> where T : class, IEntity, new()
{
    private readonly IQueryable<T> query;
    private readonly List<T> data;

    private QueryStub(IEnumerable<T> initialData)
    {
        data = new List<T>();
        data.AddRange(initialData);
        // This duff query implementation will have an empty item in the list (null)
        // so this stops that from causing tests to fail.
        query = data.AsQueryable().Where(x => x != null);
    }

    public Expression Expression => query.Expression;
    public Type ElementType => query.ElementType;
    public IQueryProvider Provider => query.Provider;

    public IEnumerator<T> GetEnumerator()
    {
        return query.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public static QueryStub<T> Create(params T[] items)
    {
        if (items == null)
        {
            return new QueryStub<T>(Enumerable.Empty<T>());
        }
        return new QueryStub<T>(items);
    }
}

```

Notes:

* Had to filter nulls (actual business case)
* Mocking is even hackier

```cs

public static void SetupIQueryable<T>(this Mock<T> mock, IQueryable queryable)
        where T: class, IQueryable
    {
        mock.Setup(r => r.GetEnumerator()).Returns(queryable.GetEnumerator());
        mock.Setup(r => r.Provider).Returns(queryable.Provider);
        mock.Setup(r => r.ElementType).Returns(queryable.ElementType);
        mock.Setup(r => r.Expression).Returns(queryable.Expression);
    }

```

This is a simplified solution, but it's basically a requirement that you understand a tonne of inner implementation.
Any changes to this would require changes to a test.














-------------------------------------------------------
# How to misuse the repository pattern


This is a bit of a personal retrospective on a project I worked on for my job.  There were a couple of technical issues that arose due to design decisions early on in the development but I'm mostly going to focus on what I consider our great misuse of the repository pattern and the impact that had on the project as a whole.

While I'm not too proud to state it, I think few of my colleagues would argue that we successfully created a legacy project in 6 months from the hopes of something new and greenfield.  Quite an achievment.  Fortunately we're aware that it was wrong, and our culture is one of reassessing and improvement.  We'll get better, and this is a small step towards that!

The project is a web api, it's little more than a CRUD application, really although you wouldn't know that to look at the code.  It definately suffers from a little over engineering. (something to discuss another time.)

![Over engineered](https://github.com/andrewgoacher/blogposts/blob/master/overengineered.jpg?raw=true)

We use Entity Framework and similar ORMs a lot in our software, so it was no surprise to anyone we'd continue to use this.  Entity Framework was chosen and we build an infrastructure layer around this.  Entity Framework was wrapped in what we called a repository, the infrastructure handled all the harnessing here.  We didn't really need to talk to Entity Framework directly.
We could, if we wanted, inject an `IRepository<TEntity>` into a class and gain direct access to any table on there.
`IRepository<TEntity>` also had an `IQueryable<TEntity>` contract.  We could essentially query any entity from any repository.
We had another implementation for Unit of Work and another for an `IQuery<TEntity>`, these were used for reading from the database whereas the repository would also allow writing.

A typical implementation at this point would look something like the following;

``` cs

public class Service
{
    private readonly IRepository<ServiceEntity> repository;
    private readonly IQuery<ServiceEntity> query;
    private readonly IUnitOfWork uow;
    public Service(IUnitOfWork uow, IRepository<ServiceEntity> repository, IQuery<ServiceEntity> query)
    {
        this.uow = uow;
        this.repository = repository;
        this.query = query;
    }

    public void StoryData(ServiceEntity entity)
    {
        uow.BeginTransaction();

        repository.Add(entity);
        repository.Save();

        uow.EndTransaction();
    }

    public IEnumerable<ServiceEntity> GetEntities()
    {
        return query.ToList();
    }

}

```

The project uses Entity Framework which is wrapped in an implementation of the repository pattern.  I believe the idea here was to allow us to switch out the data provider at a later date, or use multiple, without much significant change to the domain layers.  It should also have been relatively easy to test and to mock.

Intro/tldr

Talk about how the refactor came about.  This isn't to discuss bad planning.  This is about the consequences. Explain this isn't a single issue - the repository pattern correctly applied wouldn't have been a pancea.  But it would have helped.

Talk about what lessons were learned from this.  How the developers suffered, how the project suffered.  

Explain what the point of this write up is, what we hope to achieve.
- We don't want others making this mistake
- Want to highlight this was a mistake of understanding a concept
- Was a mistake to jump in 

- Want to achieve: A culture change to highlight when something is off
- A down tools mentality to fix this as it becomes an issue
- A better way of approachign TDD
- A project that is maintainable beyond it's current lifecycle

Define the pattern (Martin Fowler)
[Martin Fowler has this to say on repositoryies:](https://martinfowler.com/eaaCatalog/repository.html)
>A Repository mediates between the domain and data mapping layers, acting like an in-memory domain object collection. Client objects construct query specifications declaratively and submit them to Repository for satisfaction. Objects can be added to and removed from the Repository, as they can from a simple collection of objects, and the mapping code encapsulated by the Repository will carry out the appropriate operations behind the scenes

Code Project: [Code Project: Repository Pattern done right](https://www.codeproject.com/Articles/526874/Repository-pattern-done-right)
>Using repositories is not about being able to switch persistence technology (i.e. changing database or using a web service etc instead).

Summarise it in own words

Repository pattern should abstract away the data store implementation. Shouldn't matter if we use a database or not, shouldn't matter if we use SQL, or Mongo.
The repository doesn't care if a domain object sits on one table or 2 or across several apis. It acts as an in-memory store of domain concerned data.

Why we wanted to use this
I think initially it was supposed to wrap the data layer.
The repository wrapped Entity Framework which meant we could switch ORM providers or even use direct SQL if we wanted.  
It was also injectible and was supposed to be testable

Why we want to use this

Starts off with the need to refactor our data architecture. Not the best thing to need to do but we figured it would only be the infrastructure that has major changes.
It was painful to use and painful to maintain.  We also have a LOT of code bleeding out of the main project

We had to wrap Entity Frameworks ToListAsync methods
We had to expose linqtosql/ef syntax in methods
(leaky abstractions)
Testability - not caring about implementation of dta

Explain the sample Architecture:

Uses EF

ORMS/Repository Pattern/Endpoints

What did we do

Why was this a problem

What should we have done

Closing words?




* The definitions for Reposiotry pattern
* ORMs and the repository pattern
* Decisions around "coupling"
* MArtin Fowlers definition (use quote)



* What prompted our change
* What we did
* What went wrong
    *Do not expose LINQ methods
    *Leaky Abstractions
* What could we have done